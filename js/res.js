function toggleCollapse(id) {
    var e = document.getElementById(id);

    if (e.classList.contains('show')) {
        e.classList.replace('show', 'collapse');
    } else {
        e.classList.replace('collapse','show');
    }
}